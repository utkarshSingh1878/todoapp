// selectors
const addButton = document.querySelector(".addButton");
const inputValue = document.querySelector('.input');
const container = document.querySelector('.container');
const saved = document.querySelector(".saved")

// event listeners
addButton.addEventListener("click", () => {
    const uuid = uuidv4()
    if (inputValue.value === "") {
        alert("Please enter a todo")
    }
    else {
        let todoData = {
            text: (inputValue.value),
            completed: false
        }
        localStorage.setItem(uuid, JSON.stringify(todoData));
    }

    const task = JSON.parse(localStorage.getItem(uuid))
    // console.log(task.text)
    // console.log(task.completed)
    createDiv(task, uuid)
})


// functions
function createDiv(todoItem, uuid) {
    // creating a div with elements
    let itemBox = document.createElement("div");
    itemBox.classList.add("item");
    itemBox.classList.add("is-flex");
    itemBox.classList.add("is-justify-content-space-between");
    itemBox.classList.add("is-align-items-center");

    // creating a checkbox
    let checkBox = document.createElement("input")
    checkBox.classList.add("checked_input")
    checkBox.type = "checkbox"
    console.log("real value", todoItem.completed)
    checkBox.value = JSON.parse(todoItem.completed)
    console.log("checkbox value", checkBox.value)
    // creating todo task
    let input = document.createElement("input");
    input.value = todoItem.text;
    input.disabled = true;
    input.classList.add("input");
    input.classList.add("is-warning");
    input.classList.add("item_input");
    input.type = "text";

    // creating edit button 
    let editButton = document.createElement("button");
    editButton.innerHTML = "Edit";
    editButton.classList.add("button");
    editButton.classList.add("editButton");
    editButton.classList.add("is-warning");
    editButton.classList.add("is-small");

    // creating remove button
    let removeButton = document.createElement("button");
    removeButton.innerHTML = "Remove";
    removeButton.classList.add("button");
    removeButton.classList.add("is-danger");
    removeButton.classList.add("removeButton");
    removeButton.classList.add("is-small");

    // appending all child element in container
    container.appendChild(itemBox);
    // appending all elemnts in itemBox
    itemBox.appendChild(checkBox)
    itemBox.appendChild(input);
    itemBox.appendChild(editButton);
    itemBox.appendChild(removeButton);

    editButton.addEventListener("click", () => {
        editText(input, checkBox.value, uuid);
    })
    removeButton.addEventListener("click", () => {
        remove(itemBox, uuid);
    })
    checkBox.addEventListener("click", () => {
        checkedTodo(uuid)
    })
    inputValue.value = ""
}

function checkedTodo(uuid) {
    let todoText = JSON.parse(localStorage.getItem(uuid)).text
    let checkMark = !JSON.parse(localStorage.getItem(uuid)).completed
    localStorage.setItem(uuid, JSON.stringify({
        text: todoText,
        completed: checkMark
    }))
}
function editText(input, cbValue, uuid) {
    // console.log(input)
    input.disabled = !input.disabled;
    localStorage.setItem(uuid, JSON.stringify({
        text: (input.value),
        completed: JSON.parse(cbValue)
    }))
}

function remove(itemBox, uuid) {
    container.removeChild(itemBox);
    localStorage.removeItem(uuid);
}

Object.entries(localStorage).forEach((task) => {
    // console.log(task)
    createDiv(JSON.parse(task[1]), task[0])
})

// for (var i = 0; i < localStorage.length; i++) {
//     // console.log(localStorage.getItem(localStorage.key(i)));
//     let task = localStorage.getItem(localStorage.key(i))
// }